import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class SelenideTest {

    @Test
    public void selenideTest() {
        open("https://kharkiv.ithillel.ua/");
    }

    @Test
    public void selenideTest1() {
        open("https://kharkiv.ithillel.ua/");
        $(By.xpath("//*[@id=\"signCoursesButton\"]")).click();
        $(By.xpath("/html/body/div[7]/div/form/div/div[2]")).should(Condition.appear);
    }

    @Test
    public void selenideTest2() {
        open("https://kharkiv.ithillel.ua/");
        $(By.xpath("/html/body/div[7]/div/form/div/header/h3")).should(Condition.exactText("Заявка на регистрацию"));
    }

    @Test
    public void selenideTest3() {
        open("https://kharkiv.ithillel.ua/");
        $(By.xpath("//*[@id=\"course\"]")).click();
        $(By.xpath("/html/body/header[1]/div/nav[2]/section/div/div/div[1]")).shouldBe(Condition.visible);
        $(By.xpath("//*[@id=\"course\"]")).click();
        $(By.xpath("/html/body/header[1]/div/nav[2]/section/div/div/div[1]")).shouldBe(Condition.disappear);
    }

    @Test
    public void selenideTest4() {
        open("https://kharkiv.ithillel.ua/");
        $(By.xpath("//*[@id=\"course\"]")).click();
        $(By.xpath("//*[@id=\"searchInput\"]")).setValue("Manual");
        $(By.xpath("/html/body/header[1]/div/nav[2]/section/div/div/div[3]/div[2]/div[1]/a/div/h3")).should(Condition.matchText("Manual"));

    }

}
